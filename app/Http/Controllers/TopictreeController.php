<?php

namespace App\Http\Controllers;

use App\Models\Topictree;
use Illuminate\Http\Request;

class TopictreeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Topictree  $topictree
     * @return \Illuminate\Http\Response
     */
    public function show(Topictree $topictree)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Topictree  $topictree
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Topictree $topictree)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Topictree  $topictree
     * @return \Illuminate\Http\Response
     */
    public function destroy(Topictree $topictree)
    {
        //
    }
}
