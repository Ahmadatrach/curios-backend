<?php

namespace App\Http\Controllers;

use App\Models\EligibleUser;
use Illuminate\Http\Request;

class EligibleUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EligibleUser  $eligibleUser
     * @return \Illuminate\Http\Response
     */
    public function show(EligibleUser $eligibleUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EligibleUser  $eligibleUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EligibleUser $eligibleUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EligibleUser  $eligibleUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(EligibleUser $eligibleUser)
    {
        //
    }
}
