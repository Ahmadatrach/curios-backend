<?php

namespace App\Http\Controllers;

use App\Models\Profiletopic;
use Illuminate\Http\Request;

class ProfiletopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Profiletopic  $profiletopic
     * @return \Illuminate\Http\Response
     */
    public function show(Profiletopic $profiletopic)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Profiletopic  $profiletopic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profiletopic $profiletopic)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Profiletopic  $profiletopic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profiletopic $profiletopic)
    {
        //
    }
}
