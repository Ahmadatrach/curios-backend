<?php

namespace App\Http\Controllers;

use App\Models\Doctopic;
use Illuminate\Http\Request;

class DoctopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Doctopic  $doctopic
     * @return \Illuminate\Http\Response
     */
    public function show(Doctopic $doctopic)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Doctopic  $doctopic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Doctopic $doctopic)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Doctopic  $doctopic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Doctopic $doctopic)
    {
        //
    }
}
