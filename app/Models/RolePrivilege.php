<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RolePrivilege extends Model
{
    use HasFactory;
    public function userRole()
    {
        return $this->belongsTo(UserRoles::class, 'user_role_id');
    }
    public function privilege()
    {
        return $this->belongsTo(Privilege::class, 'privilege_id');
    }
}
