<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('mobile')->nullable();
            $table->string('landline')->nullable();
            $table->string('address')->nullable();
            $table->string('biography')->nullable();
            $table->integer('status')->nullable();
            $table->integer('gender')->nullable();
            $table->Date('birthdate')->nullable();
            $table->string('email')->unique();
            $table->string('secondemail')->unique()->nullable();
            $table->string('firebaseid')->unique()->nullable();
            $table->string('password');
            $table->unsignedBigInteger("country_id")->nullable();
            $table->foreign('country_id')->references('id')->on('countries')->onUpdate('cascade')->onDelete('cascade');
            $table->unsignedBigInteger("user_role_id")->nullable();
            $table->foreign('user_role_id')->references('id')->on('user_roles')->onUpdate('cascade')->onDelete('cascade');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
